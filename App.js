import React, { Component } from 'react';

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
const Stack = createStackNavigator()

import screen1 from './pages/screen1'
import screen2 from './pages/screen2'
class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="screen1">
          <Stack.Screen name="screen1" component={screen1} options={{ headerShown: false, gestureEnabled: false }} />
          <Stack.Screen name="screen2" component={screen2} options={{ headerShown: false, gestureEnabled: false }} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
};



export default App;