
import React from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import Button from '../components/button'
import Styles from '../common/styles'

class screen2 extends React.Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }


    render() {
        return (
            <SafeAreaView style={Styles.container} >
                <Button
                    title="Take me back"
                    onPress={() => this.props.navigation.goBack()}
                />
            </SafeAreaView>
        )
    }
}
export default screen2