
import React, { useRef } from 'react';
import {
    View,
} from 'react-native';
import BottomSheet from '../components/bottomSheet'
import Button from '../components/button'

import Styles from '../common/styles'

function screen1(props) {
    const childRef = useRef();

    return (
        <View style={Styles.container}>
            <Button
                title="Open"
                onPress={() => childRef.current.showBottomSheet()}
            />

            <BottomSheet
                ref={childRef}
                navigation={props.navigation}>
            </BottomSheet>
        </View>
    )
}
export default screen1