import React, { useState, forwardRef, useImperativeHandle } from 'react'
import {
    Animated,
    View,
    ScrollView,
    Text,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    Image,
} from 'react-native'
const services = [{
    "title": "Transfer Cash",
    "desc": "Add and withdraw cash",
    "icon": require("../images/Group21.png")
}, {
    "title": "Save for something new",
    "desc": "Save & invest towords something in the future",
    "icon": require("../images/Group20.png")
}, {
    "title": "Invite Cameraman",
    "desc": "Give Cameraman access to login their account",
    "icon": require("../images/Group18.png")
}, {
    "title": "Share profile link",
    "desc": "Other can signup and contribute to this account.",
    "icon": require("../images/Group10.png")
}, {
    "title": "Settings and Account Documents",
    "desc": "View and change settings. Access monthly statements, trade confirms and tax docs.",
    "icon": require("../images/Group10.png")
}, {
    "title": "Delete Account",
    "desc": "Remove an account that is not in use.",
    "icon": require("../images/Group22.png")
}]

const { height, width } = Dimensions.get("screen")

const bottomSheet = forwardRef((props, ref) => {
    const [alignment] = useState(new Animated.Value(0))
    const openBottomSheet = () => {
        Animated.timing(alignment, {
            toValue: 1,
            duration: 500,
        }).start();
        console.log("-------", services)
    }

    const closeBottomSheet = () => {
        Animated.timing(alignment, {
            toValue: 0,
            duration: 500
        }).start()
    }

    const bottomSheetInterpolate = alignment.interpolate({
        inputRange: [0, 1],
        outputRange: [-height / 2 - 50, 0]
    })

    const bottomSheetStyle = {
        bottom: bottomSheetInterpolate
    }

    const eventHandler = (e) => {
        if (e.nativeEvent.contentOffset.y > 0) {
            openBottomSheet()
        } else if (e.nativeEvent.contentOffset.y <= 0) {
            closeBottomSheet()
        }
    }

    useImperativeHandle(
        ref,
        () => ({
            showBottomSheet() {
                openBottomSheet()
            }
        }),
    )

    const navigateToNextScreen = () => {
        const { navigation } = props
        navigation.navigate('screen2')
    }

    const renderItem = (item, index) => {
        return <><TouchableOpacity
            key={item.title + index}
            onPress={navigateToNextScreen}
            style={styles.items} >
                <Image
                    style={styles.avatarView}
                    source={item.icon}
                />
            <View style={styles.txtView}>
                <Text style={styles.txt}>{item.title}</Text>
                <Text  style={[styles.txt, styles.desc]}>{item.desc}</Text>
            </View>
        </TouchableOpacity>
        <View style={styles.devider}/>
        </>
    }


    return (
        <Animated.View style={[styles.wrapper, bottomSheetStyle]}>
            <View>
                <ScrollView
                    onScroll={eventHandler}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ alignItems: 'center' }}
                    style={styles.scrollView}>
                    {services.map((item, index) => {
                        return (renderItem(item, index))
                    })}
                </ScrollView>
            </View>
        </Animated.View>
    )
})



export default bottomSheet

const styles = StyleSheet.create({
    wrapper: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        backgroundColor: '#f4f5f6',
        height: height / 1.5 - 100,
        width: width

    },
    scrollView: {
        width: '100%',
        paddingTop: 15,
        marginBottom: 25,
    },
    items: {
        marginTop: 1,
        minHeight: 120,
        width: '85%',
        borderBottomColor: '#fff',
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    devider: {
      height: 1,
      width: '100%',
      backgroundColor: '#fff'
    },
    avatarView: {
        height: 65,
        width: 65
    },
    txtView: {
        justifyContent: 'space-evenly',
        marginLeft: 20,
        flex: 1,
        paddingVertical: 10
    },
    txt: {
        fontSize: 17,
        color: '#000',
        fontFamily: 'Manrope-Bold',
    },
    desc: {
        fontSize: 15,
        marginTop: 7,
        fontWeight: '500',
        fontFamily: 'Manrope-Regular',
        color: '#767C81',
    }

})