import React, { useState, forwardRef, useImperativeHandle } from 'react'
import {
    TouchableOpacity,
    Text,
    StyleSheet,
} from 'react-native'
import Styles from '../common/styles'

const button = forwardRef((props, ref) => {
    return (
        <TouchableOpacity
            onPress={() => props.onPress()}
            style={Styles.button}>
            <Text style={Styles.btnTitle}>{props.title}</Text>
        </TouchableOpacity>
    )
})



export default button

const styles = StyleSheet.create({


})