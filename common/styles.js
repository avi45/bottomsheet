import { StyleSheet, Dimensions } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    button: {
        height: 50,
        width: 200,
        marginTop: 100,
        backgroundColor: '#DEDEDE',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnTitle: {
        color: '#000',
        fontSize: 18,
        fontFamily: 'Manrope-Bold',
    }
})

export default styles